package gl.practice.springsec;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/")
    public String testCall(){
        return "<h1>Hello</h1>";
    }
}
